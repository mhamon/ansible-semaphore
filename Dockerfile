FROM docker.io/semaphoreui/semaphore:v2.9.4
USER root
RUN apk update && apk add --no-cache py3-dnspython py3-netaddr py3-jmespath
USER semaphore
