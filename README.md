# Ansible Semaphore

Rebuild de l'image Docker officiel de [ansible-semaphore](https://www.ansible-semaphore.com/) pour rajouter des librairies Python nécessaires à savoir :
 * dnspython
 * netaddr
 * jmespath
